import React from 'react';
import Image from '../utils/Image';
import Education from './Education';
import Experience from './Experience';
import Courses from './Courses';
import Profile from './Profile';
import Skills from './Skills';

function CurriculumVitae() { 

  return (
    <section>
      <div className="max-w-6xl mx-auto px-4 sm:px-6">
        <div className="py-12 md:py-20 border-t border-gray-800">

            {/* Section content */}
            <div className="mb-4">
                <div className="flex justify-center items-center">
                  <Image className="rounded-full" src={require('../images/profile.png')} width={300} alt="Open" />
                </div>
                <div className="text-center py-6">
                  <p className="h2 mb-4">Siim Vaus</p>
                  <p className="text-xl text-gray-400 mb-6">Software Developer</p>
                </div>
            </div>

            <div className="flex mb-4">
                <div className="rounded-md w-full bg-gray-800">
                  <Profile />
                </div>
            </div>

            <div className="flex mb-4">
                <div className="rounded-md w-full bg-gray-800">
                  <Experience />
                </div>
            </div>

            <div className="flex mb-4">
                <div className="rounded-md w-full bg-gray-800">
                  <Education />
                </div>
            </div>

            <div className="flex mb-4">
                <div className="rounded-md w-full bg-gray-800">
                  <Courses />
                </div>
            </div>

            <div className="flex mb-4">
                <div className="rounded-md w-full bg-gray-800">
                  <Skills />
                </div>
            </div>

        </div>
      </div>
    </section>
  );
}

export default CurriculumVitae;
