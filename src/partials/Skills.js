import React from 'react';

function Skills() {
  return (
    <section>
      <div className="max-w-6xl mx-auto px-4 sm:px-6">
        <div className="py-12 md:py-20">

          {/* Section header */}
          <div className="max-w-3xl mx-auto text-center pb-12 md:pb-20">
            <h2 className="h2 mb-4">Skills</h2>
          </div>

          {/* Items */}
          <div className="max-w-3xl mx-auto -my-4 md:-my-6" data-aos-id-timeline>

          <div className="relative pt-1">
            <div className="flex mb-2 items-center justify-between">
              <div>
                <span className="text-xs font-semibold inline-block py-1 px-2 uppercase rounded-full text-red-600 bg-red-200">
                  HTML
                </span>
              </div>
              <div className="text-right">
                <span className="text-xs font-semibold inline-block text-red-600">
                  75%
                </span>
              </div>
            </div>
            <div className="overflow-hidden h-2 mb-4 text-xs flex rounded bg-red-200">
              <div style={{ width: "75%" }} className="shadow-none flex flex-col text-center whitespace-nowrap text-red justify-center bg-red-500"></div>
            </div>
          </div>

          <div className="relative pt-1">
            <div className="flex mb-2 items-center justify-between">
              <div>
                <span className="text-xs font-semibold inline-block py-1 px-2 mr-1 uppercase rounded-full text-blue-600 bg-blue-200">
                  CSS
                </span>
                <span className="text-xs font-semibold inline-block py-1 px-2 mr-1 uppercase rounded-full text-blue-600 bg-blue-200">
                  Bootstrap
                </span>
                <span className="text-xs font-semibold inline-block py-1 px-2 mr-1 uppercase rounded-full text-blue-600 bg-blue-200">
                  Tailwind CSS
                </span>
                <span className="text-xs font-semibold inline-block py-1 px-2 mr-1 uppercase rounded-full text-blue-600 bg-blue-200">
                  Materialize
                </span>
                <span className="text-xs font-semibold inline-block py-1 px-2 mr-1 uppercase rounded-full text-blue-600 bg-blue-200">
                  Material UI
                </span>
                <span className="text-xs font-semibold inline-block py-1 px-2 uppercase rounded-full text-blue-600 bg-blue-200">
                  Angular Material
                </span>
              </div>
              <div className="text-right">
                <span className="text-xs font-semibold inline-block text-blue-600">
                  70%
                </span>
              </div>
            </div>
            <div className="overflow-hidden h-2 mb-4 text-xs flex rounded bg-blue-200">
              <div style={{ width: "70%" }} className="shadow-none flex flex-col text-center whitespace-nowrap text-blue justify-center bg-blue-500"></div>
            </div>
          </div>

          <div className="relative pt-1">
            <div className="flex mb-2 items-center justify-between">
              <div>
                <span className="text-xs font-semibold inline-block py-1 px-2 mr-1 uppercase rounded-full text-yellow-600 bg-yellow-200">
                  JavaScript
                </span>
                <span className="text-xs font-semibold inline-block py-1 px-2 mr-1 uppercase rounded-full text-red-600 bg-red-200">
                  Angular
                </span>
                <span className="text-xs font-semibold inline-block py-1 px-2 uppercase rounded-full text-blue-600 bg-blue-200">
                  React
                </span>
              </div>
              <div className="text-right">
                <span className="text-xs font-semibold inline-block text-yellow-600">
                  50%
                </span>
              </div>
            </div>
            <div className="overflow-hidden h-2 mb-4 text-xs flex rounded bg-yellow-200">
              <div style={{ width: "50%" }} className="shadow-none flex flex-col text-center whitespace-nowrap text-yellow justify-center bg-yellow-500"></div>
            </div>
          </div>

          <div className="relative pt-1">
            <div className="flex mb-2 items-center justify-between">
              <div>
                <span className="text-xs font-semibold inline-block py-1 px-2 mr-1 uppercase rounded-full text-blue-600 bg-blue-200">
                  Golang
                </span>
                <span className="text-xs font-semibold inline-block py-1 px-2 mr-1 uppercase rounded-full text-blue-600 bg-blue-200">
                  Mux
                </span>
                <span className="text-xs font-semibold inline-block py-1 px-2 mr-1 uppercase rounded-full text-blue-600 bg-blue-200">
                  Echo
                </span>
                <span className="text-xs font-semibold inline-block py-1 px-2 mr-1 uppercase rounded-full text-blue-600 bg-blue-200">
                  Gorm
                </span>
              </div>
              <div className="text-right">
                <span className="text-xs font-semibold inline-block text-blue-600">
                  55%
                </span>
              </div>
            </div>
            <div className="overflow-hidden h-2 mb-4 text-xs flex rounded bg-blue-200">
              <div style={{ width: "55%" }} className="shadow-none flex flex-col text-center whitespace-nowrap text-blue justify-center bg-blue-500"></div>
            </div>
          </div>

          <div className="relative pt-1">
            <div className="flex mb-2 items-center justify-between">
              <div>
                <span className="text-xs font-semibold inline-block py-1 px-2 mr-1 uppercase rounded-full text-red-600 bg-red-200">
                  Java
                </span>
                <span className="text-xs font-semibold inline-block py-1 px-2 uppercase rounded-full text-green-600 bg-green-200">
                  Spring
                </span>
              </div>
              <div className="text-right">
                <span className="text-xs font-semibold inline-block text-red-600">
                  50%
                </span>
              </div>
            </div>
            <div className="overflow-hidden h-2 mb-4 text-xs flex rounded bg-red-200">
              <div style={{ width: "50%" }} className="shadow-none flex flex-col text-center whitespace-nowrap text-red justify-center bg-red-500"></div>
            </div>
          </div>

          <div className="relative pt-1">
            <div className="flex mb-2 items-center justify-between">
              <div>
                <span className="text-xs font-semibold inline-block py-1 px-2 mr-1 uppercase rounded-full text-blue-600 bg-blue-200">
                  C#
                </span>
                <span className="text-xs font-semibold inline-block py-1 px-2 uppercase rounded-full text-blue-600 bg-blue-200">
                  .NET
                </span>
              </div>
              <div className="text-right">
                <span className="text-xs font-semibold inline-block text-blue-600">
                  40%
                </span>
              </div>
            </div>
            <div className="overflow-hidden h-2 mb-4 text-xs flex rounded bg-blue-200">
              <div style={{ width: "40%" }} className="shadow-none flex flex-col text-center whitespace-nowrap text-red justify-center bg-blue-500"></div>
            </div>
          </div>

          <div className="relative pt-1">
            <div className="flex mb-2 items-center justify-between">
              <div>
                <span className="text-xs font-semibold inline-block py-1 px-2 mr-1 uppercase rounded-full text-gray-600 bg-gray-200">
                  SQL
                </span>
                <span className="text-xs font-semibold inline-block py-1 px-2 mr-1 uppercase rounded-full text-gray-600 bg-gray-200">
                  MySQL
                </span>
                <span className="text-xs font-semibold inline-block py-1 px-2 mr-1 uppercase rounded-full text-gray-600 bg-gray-200">
                  Postgres
                </span>
                <span className="text-xs font-semibold inline-block py-1 px-2 uppercase rounded-full text-gray-600 bg-gray-200">
                  SQLite
                </span>
              </div>
              <div className="text-right">
                <span className="text-xs font-semibold inline-block text-gray-600">
                  65%
                </span>
              </div>
            </div>
            <div className="overflow-hidden h-2 mb-4 text-xs flex rounded bg-gray-200">
              <div style={{ width: "65%" }} className="shadow-none flex flex-col text-center whitespace-nowrap text-gray justify-center bg-gray-500"></div>
            </div>
          </div>

          <div className="relative pt-1">
            <div className="flex mb-2 items-center justify-between">
              <div>
                <span className="text-xs font-semibold inline-block py-1 px-2 uppercase rounded-full text-blue-600 bg-blue-200">
                  Docker
                </span>
              </div>
              <div className="text-right">
                <span className="text-xs font-semibold inline-block text-blue-600">
                  70%
                </span>
              </div>
            </div>
            <div className="overflow-hidden h-2 mb-4 text-xs flex rounded bg-blue-200">
              <div style={{ width: "70%" }} className="shadow-none flex flex-col text-center whitespace-nowrap text-blue justify-center bg-blue-500"></div>
            </div>
          </div>

          </div>

        </div>
      </div>
    </section>
  );
}

export default Skills;