import React from 'react';
import { Link } from 'react-router-dom';
import { FaBitbucket, FaGithub, FaLinkedin } from 'react-icons/fa'
import Image from '../utils/Image';

const scrollUp = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' })
}

function SimpleFooter() {
  return (
    <footer>
      <div className="py-12 md:py-16">
        <div className="max-w-6xl mx-auto px-4 sm:px-6">

            <div className="flex justify-center mb-3 md:justify-start">
                <Image src={require('../images/logo.png')} alt="Open" width={41} height={41} />
            </div>

            <div className="flex justify-center mb-2 md:justify-end">
                <div className="">
                    <ul className="flex">
                        <li className="m-3">
                            <a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/siimvaus/">
                                <FaLinkedin size="2em" />
                            </a>
                        </li>
                        <li className="m-3">
                            <a target="_blank" rel="noopener noreferrer" href="https://github.com/Wauchy">
                                <FaGithub size="2em" />
                            </a>
                        </li>
                        <li className="m-3">
                            <a target="_blank" rel="noopener noreferrer" href="https://bitbucket.org/waushop/">
                                <FaBitbucket size="2em" />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

        <div className="">
            <nav className="">
                <ul className="flex justify-center mb-3 md:justify-end">
                    <li className="m-3">
                    <Link to="/" onClick={scrollUp}>Home</Link>
                    </li>
                    <li className="m-3">
                    <Link to="/resume" onClick={scrollUp}>Resume</Link>
                    </li>
                    <li className="m-3">
                    <Link to="/portfolio" onClick={scrollUp}>Portfolio</Link>
                    </li>
                </ul>
            </nav>
            <div className="text-center">&copy; 2020 <a href="https://waushop.ee">Waushop</a>. All right reserved</div>
        </div>

        </div>
      </div>
    </footer>
  );
}

export default SimpleFooter;
