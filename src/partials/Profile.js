import React from 'react';

function Profile() {
  return (
    <section>
      <div className="max-w-6xl mx-auto px-4 sm:px-6">
        <div className="py-12 md:py-20">

          {/* Section header */}
          <div className="max-w-3xl mx-auto text-center pb-12 md:pb-20">
            <h2 className="h2 mb-4">Summary</h2>
          </div>
          
          {/* Items */}
          <div className="max-w-3xl mx-auto -my-4 md:-my-6" data-aos-id-timeline>
            <p className="text-xl text-gray-400 text-justify mb-6">I am a software engineer working on both web and desktop applications. I started my professional career as a developer 1 year ago when I took part of C# and .NET backend development course. Till then I was working as a freelancer besides my main profession which was a maritime officer. But tinkering I’ve been since I was a kid.</p>
            <p className="text-xl text-gray-400 text-justify">On my present position I’m building RESTful web services in Golang and user interfaces with JavaScript. I'm familiar with React and Angular, I know how jQuery and Node.js can be useful for me and I have touched a bit Java aswell.</p>
          </div>

        </div>
      </div>
    </section>
  );
}

export default Profile;