import React from 'react';

import Header from '../partials/Header';
//import PageIllustration from '../partials/PageIllustration';
import HeroHome from '../partials/HeroHome';
import FeaturesBlocks from '../partials/FeaturesBlocks';
import CtaContact from '../partials/CtaContact';
import SimpleFooter from '../partials/SimpleFooter';

function Home() {
  return (
    <div className="flex flex-col min-h-screen overflow-hidden">

      {/*  Site header */}
      <Header />

      {/*  Page content */}
      <main className="flex-grow">

        {/*  Page illustration */}
        {/*<div className="relative max-w-6xl mx-auto h-0 pointer-events-none" aria-hidden="true">
          <PageIllustration />
        </div>*/}

        {/*  Page sections */}
        <HeroHome />
        <FeaturesBlocks />
        <CtaContact />


      </main>

      {/*  Site footer */}
      <SimpleFooter />

    </div>
  );
}

export default Home;